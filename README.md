# Voltage Regulator.

The voltage regulator takes in voltage from the battery and keep it at a constant 0-3.3V and out put to the PIHat.

**How to use the circuit**
Step 1
- Make sure the voltage regulator circuit is connected to the power supply (24V Battery)

Step 2
Connect the output of the voltage regulator to the 0-3.3V input of the PiHat and start using tour Hat.

FQA
Is the output directly connected to the PIHat?
- The output can be connected directly to the PIHat since it tested and confirmed that the voltage regulator does not prove more than 3.3V that will damage the PiHat.
Will replacing the supply battery voltage cause the circuit to output correct voltage?
- If the battery isnt supplying 24V, the circuit can still output the same results as long as the zener diode is not changed.


**Matters to keep in mind and can assist improve the design and know the operating condtions**
- Do not over power the circuit (If considere changing the battery)
- There is also no circuitory to protect the circuit from over current
- The circuit had no protection to make it fit to operate in different temperature conditions. Make sure the temperature of the circuit together with components is around room temperature/normal operating conditions.

**Design conclusion**

The design is not fully satisfying. It outputs the required voltages which is what it was meant to achieve but other specification conditions were not considered since the circuit is perfect under normal conditions. The circuit is designed to intake different DC voltages but outputs 0-3.3V, this specification is fully met and complies with the MicroHat specifications. 
The environmental and provision of the providing excess voltage to the circuit was overlooked. The circuit was not designed to withstand changes in temperature and there is no circuit that protect the circuit from overcurrent and overvoltage. The circuit cannot work perfectly if the normal conditions are altered.
